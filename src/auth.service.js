import { axios } from 'boot/axios'
import crypto from 'crypto'
import helper from '../helper'
import JwtService from './jwt.service'

const AUTH_STATE_KEY = 'auth.state'
const AUTH_VERIFIER_KEY = 'auth.verifier'
const AUTH_CERTIFICATE_KEY = 'auth.certificate'

const AuthService = {
    checkAuth () {
        return new Promise((resolve, reject) => {
            let data = JwtService.getToken()
            if (data === null) {
                return reject('JWT is missing')
            }

            this.checkRefresh()
                .then(() => {
                    return JwtService.verify(this.getCertificate()) ? resolve() : reject('JWT could not be verified')
                })
                .catch(error => {
                    reject(error)
                })
        })
    },

    isAuthenticated () {
        let data = JwtService.getToken()
        if (data === null) {
            return false
        }

        return JwtService.verify(this.getCertificate())
    },

    login () {
        return new Promise(resolve => {
            this.loadCertificate()
                .then(() => {
                    let options = helper.centerScreenOptions({ height: 800, width: 800 })
                    let handle = window.open(this.getAuthUrl(), '_blank', 'toolbar=yes,scrollbars=yes,resizable=yes,top=' + options.top + ',left=' + options.left + ',width=' + options.width + ',height=' + options.height)
                    let timer = setInterval(() => {
                        if (handle.closed) {
                            this.clearRequestStates()
                            clearInterval(timer)
                            resolve()
                        }
                    }, 1000)
                })
                .catch(error => {
                    throw new Error('Could not get OAuth Certificate from Server: ' + error.message)
                })
        })
    },

    callback (state, code) {
        return new Promise(resolve => {
            // Check saved state vs request state
            if (this.getState() !== state) {
                localStorage.removeItem(AUTH_STATE_KEY)
                localStorage.removeItem(AUTH_VERIFIER_KEY)

                throw new Error('Saved oauth state mismatched')
            }

            axios.post(process.env.AUTH_TOKEN_URL, {
                grant_type: 'authorization_code',
                client_id: process.env.AUTH_CLIENT_ID,
                redirect_uri: process.env.AUTH_CALLBACK_URI,
                code: code,
                code_verifier: this.getVerifier()
            })
                .then(response => {
                    this.clearRequestStates()

                    if (!JwtService.verify(this.getCertificate(), response.data)) {
                        throw new Error('Callback Token Error: Token could not be verified')
                    }

                    JwtService.setToken(response.data)

                    resolve(response)
                })
                .catch(error => {
                    this.clearRequestStates()

                    throw new Error('Error while trying to get a token: ' + error.message)
                })
        })
    },

    checkRefresh () {
        if (JwtService.willExpire()) {
            return this.refreshToken()
                .then(response => {
                })
                .catch(error => {
                    return Promise.reject(error)
                })
        }

        return Promise.resolve()
    },

    refreshToken () {
        let data = JwtService.getToken()
        if (data === null) {
            return Promise.reject('No Refresh Token available')
        }

        return axios.post(process.env.AUTH_TOKEN_URL, {
            grant_type: 'refresh_token',
            client_id: process.env.AUTH_CLIENT_ID,
            refresh_token: JwtService.getToken().refresh_token,
            code_verifier: helper.base64URLEncode(crypto.randomBytes(32)),
            scope: 'profile'
        })
            .then(response => {
                if (!JwtService.verify(this.getCertificate(), response.data)) {
                    throw new Error('Refresh Token Error: Token could not be verified')
                }

                JwtService.setToken(response.data)

                return response
            })
            .catch(error => {
                throw new Error('Refresh Token Error: ' + error.message)
            })
    },

    logout () {
        JwtService.destroyToken()
        localStorage.removeItem(AUTH_CERTIFICATE_KEY)
    },

    getAuthUrl () {
        let uuid = require('uuid')
        let nonce = uuid.v4()

        // PCKE
        let verifier = helper.base64URLEncode(crypto.randomBytes(32))
        let challenge = helper.base64URLEncode(helper.sha256(verifier))

        localStorage.setItem(AUTH_STATE_KEY, nonce)
        localStorage.setItem(AUTH_VERIFIER_KEY, verifier)

        let params = {
            prompt: 'login',
            scope: 'profile',
            response_type: 'code',
            approval_prompt: 'auto',
            redirect_uri: process.env.AUTH_CALLBACK_URI,
            client_id: process.env.AUTH_CLIENT_ID,
            state: nonce,
            code_challenge: challenge,
            code_challenge_method: 'S256'
        }

        const ret = []
        for (let k in params) {
            ret.push(encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        }

        return process.env.AUTH_AUTHORIZE_URL + '?' + ret.join('&')
    },

    clearRequestStates () {
        localStorage.removeItem(AUTH_STATE_KEY)
        localStorage.removeItem(AUTH_VERIFIER_KEY)
    },

    loadCertificate () {
        if (localStorage.getItem(AUTH_CERTIFICATE_KEY) !== null) {
            return Promise.resolve(localStorage.getItem(AUTH_CERTIFICATE_KEY))
        }

        return axios.get(process.env.AUTH_CERT_URL)
            .then(response => {
                localStorage.setItem(AUTH_CERTIFICATE_KEY, response.data)

                return response
            })
            .catch(error => {
                throw error
            })
    },

    getCertificate () {
        return localStorage.getItem(AUTH_CERTIFICATE_KEY)
    },

    getState () {
        return localStorage.getItem(AUTH_STATE_KEY)
    },

    getVerifier () {
        return localStorage.getItem(AUTH_VERIFIER_KEY)
    },

    loadCurrentUser () {
        let data = JwtService.getToken()
        if (data === null) {
            return Promise.reject('No JWT available')
        }

        axios.defaults.baseURL = ''
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.access_token

        return axios.get(process.env.AUTH_USER_URL)
            .catch(error => {
                throw new Error('AuthService Error: ' + error.message)
            })
    }
}

export default AuthService
