import { Base64 } from 'js-base64'
import crypto from 'crypto'

const Helper = {
    centerScreenOptions (options) {
        let documentElement = document.documentElement

        let dualScreenTop = typeof window.screenTop !== 'undefined' ? window.screenTop : screen.top
        let height = screen.height || window.innerHeight || documentElement.clientHeight
        options.top = parseInt((height - options.height) / 2, 10) + dualScreenTop

        let dualScreenLeft = typeof window.screenLeft !== 'undefined' ? window.screenLeft : screen.left
        let width = screen.width || window.innerWidth || documentElement.clientWidth
        options.left = parseInt((width - options.width) / 2, 10) + dualScreenLeft

        return options
    },

    base64URLEncode (str) {
        return Base64.encode(str)
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '')
    },

    sha256 (buffer) {
        return crypto.createHash('sha256').update(buffer).digest()
    }
}

export default Helper
